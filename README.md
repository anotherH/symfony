# UF2 NF2 PT3 #

## Imatge ##
![Captura de la app funcionando](captura.png)

## Página del manual seguido ##

[How To Deploy a Symfony Application to Production on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04)

## Errors del manual ##

* En el paso 1: sudo apt-get install git php5-cli php5-curl acl
> Utilizar la versión 7..


* en el paso 2: sudo nano /etc/mysql/my.cnf
> No es este fichero de configuración. Es el /etc/mysql/mysql.conf.d/mysqld.cnf

* En el paso 5: composer install --no-dev --optimize-autoloader
> No utilizar la ip 127.0.0.1 o la ip real. Utilizar localhost. Sino, salta error de conección.

* En el paso 7:
> Verificar que se tiene el mod de php habilitado sino la app no funciona y sale solo el codigo php.



